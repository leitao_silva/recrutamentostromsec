# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('conteudo', '0002_auto_20151106_1720'),
    ]

    operations = [
        migrations.AddField(
            model_name='ator',
            name='foto',
            field=models.ImageField(default=None, help_text='Fa\xe7a o upload da foto do ator/atriz.', upload_to=b'uploads/atores'),
            preserve_default=False,
        ),
    ]
