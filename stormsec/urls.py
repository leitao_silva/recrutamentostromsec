from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns(
    '',
    url(r'^$', 'conteudo.views.home', name='home'),
    url(r'^genero/(?P<slug_titulo>[\w_-]+)/', 'conteudo.views.home', name="home"),
    url(r'^filme/(?P<slug_titulo>[\w_-]+)/', 'conteudo.views.filme', name="filme"),
    url(r'^ator/(?P<slug_nome>[\w_-]+)/', 'conteudo.views.ator', name="ator"),

    url(r'^grappelli/', include( 'grappelli.urls' ) ),

    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
