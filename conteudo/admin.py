from django.contrib import admin
from conteudo.models import Genero,Ator, Filme


class GeneroAdmin(admin.ModelAdmin):
    search_fields = ('titulo',)


class AtorAdmin(admin.ModelAdmin):
    list_display = ('nome', 'pais',)
    search_fields = ('nome',)
    list_filter = ('pais',)


class FilmeAdmin(admin.ModelAdmin):
    list_display = ('titulo',)
    search_fields = ('titulo',)


admin.site.register(Genero, GeneroAdmin)
admin.site.register(Ator, AtorAdmin)
admin.site.register(Filme, FilmeAdmin)