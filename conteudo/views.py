# -*- encoding: utf-8 -*-

from django.http import Http404
from django.shortcuts import render
from conteudo.models import *


def home(request, slug_titulo=None):
    response_dict = {}

    filme_list = Filme.objects.all()
    if slug_titulo:
        try:
            genero = Genero.objects.get(slug=slug_titulo)
            response_dict['genero'] = genero
        except Genero.DoesNotExist:
            raise Http404

        filme_list.filter(generos=genero)

    if request.GET.has_key('ordem'):
        ordem = request.GET['ordem']
        if ordem == '2': filme_list = filme_list.order_by('-titulo')
        else: filme_list = filme_list.order_by('titulo')

    response_dict['filme_list'] = filme_list
    return render(request, 'home.html', response_dict)


def filme(request, slug_titulo):
    try:
        filme = Filme.objects.get(slug=slug_titulo)
    except Filme.DoesNotExist:
        raise Http404

    filme_list = filme.get_relacionados()
    if len(filme_list) > 20: filme_list = filme_list[:20]

    response_dict = {'filme': filme, 'filme_list':filme_list}
    return render(request, 'filme.html', response_dict)


def ator(request, slug_nome):
    try:
        ator = Ator.objects.get(slug=slug_nome)
        filmes = Filme.objects.filter(atores=ator)
        if filmes: filmes = filmes[:20]
    except Ator.DoesNotExist:
        raise Http404

    response_dict = {'ator': ator, 'filme_list': filmes}
    return render(request, 'ator.html', response_dict)
