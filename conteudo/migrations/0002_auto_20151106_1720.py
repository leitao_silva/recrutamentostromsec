# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import autoslug.fields


class Migration(migrations.Migration):

    dependencies = [
        ('conteudo', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='ator',
            name='slug',
            field=autoslug.fields.AutoSlugField(default=None, editable=False, populate_from=b'nome', unique=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='filme',
            name='slug',
            field=autoslug.fields.AutoSlugField(default=None, editable=False, populate_from=b'titulo', unique=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='genero',
            name='slug',
            field=autoslug.fields.AutoSlugField(default=None, editable=False, populate_from=b'titulo', unique=True),
            preserve_default=False,
        ),
    ]
