# -*- encoding: utf-8 -*-

from django.db import models
from Utils.country import CountryField
from autoslug import AutoSlugField


class Genero(models.Model):
    titulo = models.CharField(
        verbose_name=u'título',
        max_length=255,
        unique=True,
        help_text=u'Insira um título para o gênero de filme.'
    )

    slug = AutoSlugField(
        populate_from='titulo',
        unique=True,
    )

    def __unicode__(self):
        return self.titulo

    class Meta:
        ordering = ('titulo',)
        verbose_name = u'Gênero'
        verbose_name_plural = u"Gêneros"


class Ator(models.Model):
    nome = models.CharField(
        max_length=255,
        help_text=u'Insira o nome do ator/atriz.'
    )

    pais = CountryField(
        verbose_name=u'país',
        help_text=u'Selecione o país de origm do ator/atriz.'
    )

    foto = models.ImageField(
        upload_to='static/uploads/atores',
        help_text=u'Faça o upload da foto do ator/atriz.'
    )

    slug = AutoSlugField(
        populate_from='nome',
        unique=True,
    )

    def __unicode__(self):
        return self.nome

    class Meta:
        ordering = ('nome',)
        verbose_name = u'Ator'
        verbose_name_plural = u"Atores"


class Filme(models.Model):
    titulo = models.CharField(
        verbose_name=u'título',
        max_length=255,
        help_text=u'Insira o título do filme.'
    )

    sinopse = models.TextField(
        null=True,
        blank=True,
        help_text=u'Insira a sinopse do filme.'
    )

    capa = models.ImageField(
        upload_to='static/uploads/capas',
        help_text=u'Faça o upload da foto de capa do filme.'
    )

    generos = models.ManyToManyField(
        Genero,
        help_text=u'Selecione ou insira os gêneros do filme.'
    )

    atores = models.ManyToManyField(
        Ator,
        help_text=u'Selecione ou insira os atores ou atrizes do filme.'
    )

    slug = AutoSlugField(
        populate_from='titulo',
        unique=True,
    )

    def __unicode__(self):
        return self.titulo

    class Meta:
        ordering = ('titulo',)
        verbose_name = u'Filme'
        verbose_name_plural = u"Filmes"

    def get_generos(self):
        string = ""
        length = len(self.generos.all()) - 1

        i = 0
        for genero in self.generos.all():
            if i == 0: string += genero.titulo
            elif i == length: string += " e %s." % genero.titulo
            else: string += ", %s" % genero.titulo
            i += 1

        return string

    def get_atores(self):
        string = ""
        length = len(self.atores.all()) - 1

        i = 0
        for ator in self.atores.all():
            if i == 0: string += ator.nome
            elif i == length: string += " e %s." % ator.nome
            else: string += ", %s" % ator.nome
            i += 1

        return string

    def get_relacionados(self):
        count_dict = {}

        for ator in self.atores.all():
            for filme in Filme.objects.filter(atores=ator):
                if count_dict.has_key(filme.id): count_dict[filme.id] += 1
                else: count_dict[filme.id] = 1

        for genero in self.generos.all():
            for filme in Filme.objects.filter(generos=genero):
                if count_dict.has_key(filme.id): count_dict[filme.id] += 1
                else: count_dict[filme.id] = 1

        filme_list = []
        for key in count_dict.keys():
            filme_list.append((count_dict[key], Filme.objects.get(id=key)))

        filme_list.sort(key=lambda x: x[0])

        return filme_list




